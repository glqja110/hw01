#include <iostream>
using namespace std;

int main()
{
	int w_num; 

	cin>>w_num;
	
	if(w_num%2==1&&w_num>1)
	{		
		int i,j,pos;
		int * arr = new int[w_num * w_num]; //마방진 배열을 만든다. (예를들어 w_num이 3이였다면 3*3=9 크기의 배열을 만든다.)
		
		for(i=0;i<(w_num*w_num);i++)
		{
			arr[i]=0;   //모든 배열의 원소들을 0으로 초기화한다. (대각선 방향에 숫자가 있는지 없는지 체크를 하기 위해)
		}

		arr[w_num/2]=1;  //첫줄의 가운데 숫자를 1로 초기화 한다.

		pos=w_num/2; //'pos'는 숫자를 넣을 곳의 위치를 알려주는 변수.
		
		for(i=2;i<=((w_num) * (w_num));i++)  //숫자를 넣는 반복문. (만약 w_num이 3이였다면 2부터 9까지 반복문이 돌면서 숫자를 넣는다.)
		{
			if(pos<w_num-1) //'pos'가 마지막 열을 제외한 첫줄에 있는 경우.
 			{
				pos+=((w_num-1)*w_num)+1; // 맨위에서 대각선 방향으로 간후 맨 아래로 내리면 이런 계산 과정이 필요하다.
				arr[pos]=i; 
			}
			
			else if(pos%w_num==(w_num-1) && pos>w_num) //'pos'가 첫줄을 제외한 마지막 열에 있는 경우.
			{
				pos-=(2*w_num-1); // 맨 끝 열에서 대각선 방향으로 간후 줄의 맨 앞으로 가려면 이런 계산 과정이 필요하다.
				arr[pos]=i;
			}
			
			else if(pos==w_num-1) // 'pos'가 맨 윗줄 맨 끝 열에 있는 경우
			{
				pos+=w_num; // 아래로 한칸 내려가니 이런 계산 과정이 필요하다.
				arr[pos]=i;
			}

			else
			{
				
				if(arr[pos-(w_num-1)]!=0) //pos의 대각선 방향에 숫자가 있는 경우.
				{
					pos+=w_num; // 아래로 한칸 내려가니 이런 계산 과정이 필요하다.
					arr[pos]=i;
				}
				else //pos의 대각선 방향에 숫자가 없는 경우.
				{
					pos-=(w_num-1); //대각선 방향으로 올라가려면 이런 계산 과정이 필요하다.
					arr[pos]=i;
				}
			}
		}
			
		for(i=0;i<w_num;i++)  //배열에 마방진 숫자를 다 넣었으니 표현하는 과정.
		{
			for(j=0;j<w_num;j++)
			{
				cout<<arr[i*w_num+j]<<' ';
			}
			cout<<' '<<endl;
		}

				
		delete[] arr; //할당한건 다시 없애기
	}
	return 0;
	
}
											
				
			
		
